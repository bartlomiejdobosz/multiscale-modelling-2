package application.graingrowth.utils;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Cell {
    private Grain grain;

    private boolean inclusion;
    private boolean border;
    private double density;
    private boolean recrystallized;
    private int energy;

    private CellType type;
    private int x;
    private int y;

    public Cell(int x, int y){
        this.border = false;
        this.recrystallized = false;
        this.type = CellType.empty;
        this.x = x;
        this.y = y;
    }

    public void setGrain(Grain grain){
        if(grain != null) {
            type = CellType.grain;
        } else {
            type = CellType.empty;
        }
        this.grain = grain;
    }
}
