package application.graingrowth.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Random;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Color implements Serializable{
    private int red;
    private int green;
    private int blue;

    static Color randomColor() {
        Random random = new Random();
        Color color = new Color(random.nextInt(255),random.nextInt(255),random.nextInt(255));
        return color;
    }

    @JsonIgnore
    public javafx.scene.paint.Color getPaintColor() {
        return new javafx.scene.paint.Color(red / 255.0, green / 255.0, blue / 255.0, 1);
    }

    @JsonIgnore
    public int getRgb(){
        int rgb = red;
        rgb = (rgb << 8) + green;
        rgb = (rgb << 8) + blue;
        return rgb;
    }

    @JsonIgnore
    public void setColor(int rgb){
        red = (rgb >> 16) & 0xFF;
        green = (rgb >> 8) & 0xFF;
        blue = rgb & 0xFF;
    }

    public static Color createColor(int rgb){
        int red = (rgb >> 16) & 0xFF;
        int green = (rgb >> 8) & 0xFF;
        int blue = rgb & 0xFF;
        return new Color(red, green, blue);
    }
}
