package application.graingrowth;

import application.graingrowth.environment.*;
import application.graingrowth.file.FileExport;
import application.graingrowth.file.FileImport;
import application.graingrowth.utils.Cell;
import application.graingrowth.utils.CellType;
import application.graingrowth.utils.Grain;
import application.graingrowth.utils.Square;
import application.graingrowth.file.TImport;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.awt.*;
import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.List;

import static java.lang.Thread.sleep;

public class GrainGrowth implements Initializable {
    @FXML VBox vbox;
    @FXML TextField txtColumn; //width
    @FXML TextField txtRow; //height
    @FXML ComboBox environment;
    @FXML ToggleButton borderCondition; //bc
    @FXML TextField txtIteration;
    @FXML TextField txtGrain;
    @FXML TextField txtRadius;
    @FXML TextField inclusions;
    @FXML RadioButton inclusionsRadioButton;
    @FXML RadioButton nucelonRadioButton;
    @FXML RadioButton selectRadioButton;

    @FXML RadioButton inclusionCircleRadioButton;
    @FXML RadioButton inclusionSquareRadioButton;
    @FXML TextField inclusionRadius;
    @FXML CheckBox changeColor;
    @FXML TextField borderRadius;

    @FXML TextField txtPercent;

    @FXML CheckBox showEnergyCheckBox;
    @FXML RadioButton energyAllRadioButton;
    @FXML RadioButton energyBorderRadioButton;
    @FXML TextField txtRecrystallizedGrains;
    @FXML TextField txtStateCount;

    @FXML RadioButton radioOnStartNucleon;
    @FXML RadioButton radioConstantNucleon;
    @FXML RadioButton radioIncreasingNucleon;
    @FXML TextField txtIncreasingNucleons;


    @FXML TextField txtEnergyOnBorder;
    @FXML TextField txtEnergyAnywhere;
    Stage stage;

    FileChooser fileChooser = new FileChooser();
    List<String> extensions = new ArrayList<>();


    int height;
    int width;

    boolean bc;
    boolean grainGrowth = false;
    int iterations;

    Square squareTab[][]; //widok
    Cell cellTab[][]; //model

    Map<Integer, Grain> grains = new HashMap<>();
    int grainsNumber = 1;
    Map<Integer, Grain> selectedGrains = new HashMap<>();

    //rekrystalizacja
    final double A = 86710969050178.50;
    final double B = 9.41;


    public Stage getStage() {
        return stage;
    }
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> options = FXCollections.observableArrayList(
                "Moora"
        );
        environment.setItems(options);
        environment.setValue("Moora");

        List<FileChooser.ExtensionFilter> ex = new ArrayList<>();
        ex.add(new FileChooser.ExtensionFilter("JSON files (*.json)", "*.json"));
        ex.add(new FileChooser.ExtensionFilter("BMP files (*.bmp)", "*.bmp"));

        fileChooser.getExtensionFilters().addAll(ex);
    }

    public void createGridClick(){
        grainGrowth = false;
        this.width = Integer.parseInt(txtColumn.getText());
        this.height = Integer.parseInt(txtRow.getText());
        createGrid(width, height);
    }

    private void createGrid(int width, int height){
        cellTab = new Cell[height][width];
        squareTab = new Square[height][width];

        vbox.getChildren().clear();

        for(int i = 0; i < height; i ++) { //vbox
            HBox hbox = new HBox();
            for(int j = 0; j < width; j++) { //hbox
                cellTab[i][j] = new Cell(j,i);

                squareTab[i][j] = new Square(j,i);
                squareTab[i][j].setOnMouseClicked((event) -> {
                    Square s = (Square) event.getSource();
                    onClick(s.getXcord(),s.getYcord(), event.getButton().name());
                    updateView();
                    return;
                });

                hbox.getChildren().add(squareTab[i][j]);
            }
            vbox.getChildren().add(hbox);
        }
    }

    void  onClick(int x, int y, String type) {
        if(type.equals("PRIMARY")){
           if(nucelonRadioButton.isSelected()){
               addNewGrain(x,y);
           } else if(inclusionsRadioButton.isSelected()) {
               addNewInclusion(x, y);
           } else if(selectRadioButton.isSelected()) {
               selectGrain(x, y);
           }


        } else if(type.equals("SECONDARY")){
            if(cellTab[y][x].getType() == CellType.grain){
                deleteGrain(x,y);
            } else if(cellTab[y][x].getType() == CellType.inclusion) {
                deleteInclusion(x, y);
            } else if(cellTab[y][x].getType() == CellType.selected) {
                deselectGrain(x, y);
            }

        }
    }

    private void selectGrain(int x, int y) {
        if(cellTab[y][x].getType() == CellType.grain) {
            for(int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if(cellTab[i][j].getType() == CellType.grain && cellTab[i][j].getGrain().getId() == cellTab[y][x].getGrain().getId()){
                        cellTab[i][j].setType(CellType.selected);
                        selectedGrains.put(cellTab[i][j].getGrain().getId(), cellTab[i][j].getGrain());
                    }

                }
            }

        }
    }

    private void deselectGrain(int x, int y) {
        if(cellTab[y][x].getType() == CellType.selected) {
            for(int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if(cellTab[i][j].getType() == CellType.selected && cellTab[i][j].getGrain().getId() == cellTab[y][x].getGrain().getId()){
                        cellTab[i][j].setType(CellType.grain);
                        selectedGrains.remove(cellTab[i][j].getGrain().getId());
                    }

                }
            }
        }
    }

    private void addNewGrain(int x, int y){
        Grain newGrain = new Grain(grainsNumber);
        cellTab[y][x].setGrain(newGrain);
        grains.put(grainsNumber, newGrain);
        grainsNumber++;
    }

    private void deleteGrain(int x, int y) {
        if(cellTab[y][x].getType() == CellType.grain){
            int grainId = cellTab[y][x].getGrain().getId();
            cellTab[y][x].setGrain(null);
            grains.remove(grainId);
        }
    }

    private void addNewInclusion(int x, int y){
        int r = Integer.parseInt(inclusionRadius.getText());
        int[][] indexTab = squareCoordinates(x, y, Integer.parseInt(inclusionRadius.getText()));
        int X[] = indexTab[0];
        int Y[] = indexTab[1];

        if(grainGrowth){
            if(checkNeighbors(x, y, r)){
                if(inclusionSquareRadioButton.isSelected()) {

                    for(int i = 0; i < Y.length; i++){
                        for(int j = 0; j < X.length; j++){
                            if((Y[i] > -1 && Y[i] < height && X[j] > -1 && X[j] < width)){
                                cellTab[Y[i]][ X[j]].setType(CellType.inclusion);
                            }
                        }
                    }

                    cellTab[y][x].setType(CellType.inclusion);
                } else if(inclusionCircleRadioButton.isSelected()){
                    for(int i = 0; i < Y.length; i++){
                        for(int j = Math.abs(r - i); j < X.length - Math.abs(r - i); j++){
                            if((Y[i] > -1 && Y[i] < height && X[j] > -1 && X[j] < width)){
                                cellTab[Y[i]][ X[j]].setType(CellType.inclusion);
                            }
                        }
                    }
                }
            }
        } else {
            if(inclusionSquareRadioButton.isSelected()) {

                for(int i = 0; i < Y.length; i++){
                    for(int j = 0; j < X.length; j++){
                        if((Y[i] > -1 && Y[i] < height && X[j] > -1 && X[j] < width)){
                            cellTab[Y[i]][ X[j]].setType(CellType.inclusion);
                        }
                    }
                }

                cellTab[y][x].setType(CellType.inclusion);
            } else if(inclusionCircleRadioButton.isSelected()){
                for(int i = 0; i < Y.length; i++){
                    for(int j = Math.abs(r - i); j < X.length - Math.abs(r - i); j++){
                        if((Y[i] > -1 && Y[i] < height && X[j] > -1 && X[j] < width)){
                            cellTab[Y[i]][ X[j]].setType(CellType.inclusion);
                        }
                    }
                }
            }
        }
    }

    boolean checkNeighbors(int x, int y, int r) {
        Moor moor = new Moor();
        moor.init(width, height, bc, cellTab);
        int count = moor.numberOfNeightbors(x, y, r);
        return count > 1;
    }

    public void deleteInclusion(int x, int y){
        if(cellTab[y][x].getType() == CellType.inclusion && !grainGrowth){
            cellTab[y][x].setType(CellType.empty);
        }
    }

    public void cleanUnselected(){
        for(int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if(cellTab[i][j].getType() == CellType.grain){
                    cellTab[i][j].setType(CellType.empty);
                }

            }
        }
        updateView();
    }

    public void updateView(){
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                if(this.showEnergyCheckBox.isSelected()){

                    if(cellTab[i][j].getEnergy() == Integer.parseInt(txtEnergyOnBorder.getText())){
                        squareTab[i][j].setFill(Color.BLUE);
                    } else if (cellTab[i][j].getEnergy() == Integer.parseInt(txtEnergyAnywhere.getText())) {
                        squareTab[i][j].setFill(Color.GREEN);
                    } else {
                        squareTab[i][j].setFill(Color.RED);
                    }
                } else {
                    switch (cellTab[i][j].getType()){
                        case empty:
                            squareTab[i][j].setFill(Color.WHITE);
                            break;
                        case inclusion:
                            squareTab[i][j].setFill(Color.BLACK);
                            break;
                        case recrystallized:
                        case grain:
                            squareTab[i][j].setFill(cellTab[i][j].getGrain().getColor().getPaintColor());
                            break;
                        case selected:
                            if(this.changeColor.isSelected()) {
                                squareTab[i][j].setFill(Color.RED);
                            } else {
                                squareTab[i][j].setFill(cellTab[i][j].getGrain().getColor().getPaintColor());
                            }

                            break;
                    }
                }
            }
        }
    }

    private Environment getEnv(){
        switch(((String) environment.getValue())){
            case "Moora":
                return new Moor();
            case "von Neumana":
                return new VonNeuman();
            case "Extended moor":
                return new ExtendedMoor();
            case "Hexagoanlne lewe":
                return new HexagonalLeft();
            case "Hexagoanlne prawe":
                return new HexagonalRight();
            case "Hexagoanlne losowe":
                if(Math.random() < 0.5){
                    return new HexagonalLeft();
                }else{
                    return new HexagonalRight();
                }
            case "Pentagonalne losowe":
                double rand = Math.random();
                if(rand < 0.25){
                    return new PentagonalTop();
                }else if(rand < 0.5){
                    return new PentagonalRight();
                }else if(rand < 0.75){
                    return new PentagonalBottom();
                }else {
                    return new PentagonalLeft();
                }
            case "Losowe Automaty":
                return new RandomAutomats();
            default:
                return new Moor();
        }
    }

    public void generateRandomInclusions() {
        int inc = Integer.parseInt(inclusions.getText());

        while(inc > 0) {
            int x = (int)(Math.random() * width);
            int y = (int)(Math.random() * height);

            if(cellTab[y][x].getType() != CellType.inclusion){
                addNewInclusion(x,y);
                inc--;
            }
        }
        updateView();
    }

    public void generateRandomGerms(){
        int grainsCount = Integer.parseInt(txtGrain.getText());

        while(grainsCount > 0){
            int x = (int)(Math.random() * width);
            int y = (int)(Math.random() * height);

            if(cellTab[y][x].getType() == CellType.empty){

                addNewGrain(x,y);
                grainsCount--;
            }
            updateView();
        }
    }

    public void generateRadiusGerms(){
        int r = Integer.parseInt(txtRadius.getText());
        int grainsCount = Integer.parseInt(txtGrain.getText());
        int s = 0;

        while(grainsCount > 0 && s < 100){
            int x = (int)(Math.random() * width);
            int y = (int)(Math.random() * height);
            int indexTab[][] = squareCoordinates(x,y,r);
            int X[] = indexTab[0];
            int Y[] = indexTab[1];

            boolean clear = true;

            for(int i = 0; i < Y.length; i++){
                for(int j = Math.abs(r - i); j < X.length - Math.abs(r - i); j++){
                    if((Y[i] > -1) && (Y[i] < height) && (X[j] > -1) && (X[j] < width)){
                        if(cellTab[Y[i]][X[j]].getGrain() != null){
                            clear = false;
                        }
                    }
                }
            }
            s++;
            if(clear) {
                addNewGrain(x,y);
                s = 0;
                grainsCount--;
                updateView();
            }
        }
    }

    public void generateEvenlyGerms(){
        int r = Integer.parseInt(txtRadius.getText());
        int start = 0;
        if(r%2 == 0){
            start = 1;
        }else{
            start = 2;
        }

        for(int i = start; i < height-1; i += r) {
            for( int j = start; j < width-1; j += r){
                addNewGrain(j,i);
                updateView();
            }
        }
    }

    public void startGrainGrowth(){
        bc = borderCondition.isSelected();
        iterations = Integer.parseInt(txtIteration.getText());
        grainGrowth = true;
        int r;
        if(!txtRadius.getText().equals("")){
            r = Integer.parseInt(txtRadius.getText());
        }else{
            r = 1;
        }


        int tmp[][] = new int[height][width];

        Environment env = getEnv();
        env.init(width, height, bc, cellTab);

        env.setMoorCornersPercent(Double.parseDouble(txtPercent.getText()));

        Thread thread = new Thread(() -> {
            boolean changed = true;
            int time = 0;

            while(changed){
                System.out.println("time: " + time);
                time++;
                changed = false;
//                for(int time = 0; time < iterations; time++) {

                for (int i = 0; i < height; i++) { //inicjuje tmp
                    for (int j = 0; j < width; j++) {
                        tmp[i][j] = 0;
                    }
                }

                for (int i = 0; i < height; i++) {
                    for (int j = 0; j < width; j++) {
                        if (cellTab[i][j].getType() == CellType.empty) {
                            Map<Integer, Integer> count = env.count(j, i);
                            while(true){
                                int id = env.maxId(count);
                                if(selectedGrains.get(id) != null){
                                    count.remove(id);
                                } else {
                                    tmp[i][j] = id;
                                    break;
                                }

                                if(selectedGrains.isEmpty()){
                                    break;
                                }
                            }
                        }
                    }
                }

                for (int i = 0; i < height; i++) {
                    for (int j = 0; j < width; j++) {
                        if(tmp[i][j] != 0){
                            cellTab[i][j].setGrain(grains.get(tmp[i][j]));
                            changed = true;
                        }
                    }
                }
                updateView();
                try {
                    sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });
        thread.start();
    }

    public void recrystallization(){
        int tmp[][] = new int[height][width];
        //ustawienie granic
        for(int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if(checkBorder(j,i)){
                    cellTab[i][j].setBorder(true);
                }
            }
        }

        double criticalDensity = countDensity(0.065) /(width*height);
        System.out.println("Krytyczna " + criticalDensity);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
               for(double time = 0; time < 0.1; time += 0.001){


                        for(int i = 0; i < height; i++){
                            for(int j = 0; j < width; j++){
                                if(cellTab[i][j].isRecrystallized()){
                                    tmp[i][j] = new Integer(cellTab[i][j].getGrain().getId());
                                } else {
                                    tmp[i][j] = 0;
                                }
                            }
                        }

                        for(int i = 0; i < height; i++) {
                            for (int j = 0; j < width; j++) {

                                if (time != 0)
                                    cellTab[i][j].setDensity(countDensity(time) / (width*height) - cellTab[i][j].getDensity());
                                else
                                    cellTab[i][j].setDensity(countDensity(time) / (width * height));


                                if(cellTab[i][j].isBorder()){
                                    cellTab[i][j].setDensity(cellTab[i][j].getDensity() * (Math.random()*(1.8-1.2)+1.2));
                                }else {
                                    cellTab[i][j].setDensity(cellTab[i][j].getDensity() * (Math.random()*(1.30-1)+1));
                                }

                                if(!cellTab[i][j].isRecrystallized())
                                    tmp[i][j] = moorRec(j, i);
                            }
                        }

                        for(int i = 0; i < height; i++) {
                            for (int j = 0; j < width; j++) { //2
                                if(!cellTab[i][j].isRecrystallized()){

                                    if(tmp[i][j] != 0){
                                        cellTab[i][j].setGrain(grains.get(tmp[i][j]));
                                        cellTab[i][j].setRecrystallized(true);
                                    }else{
                                        if(cellTab[i][j].getDensity() > criticalDensity){
                                            Grain newGrain = new Grain(grainsNumber);
                                            cellTab[i][j].setGrain(newGrain);
                                            grains.put(grainsNumber, newGrain);
                                            grainsNumber++;
                                            cellTab[i][j].setRecrystallized(true);
                                        }
                                    }
                                }
                            }
                        }
                        updateView();
                        try {
                            sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        thread.start();
    }

    private boolean checkBorder(int x, int y){
        int indexTab[][] = squareCoordinates(x,y,1);
        int X[] = indexTab[0];
        int Y[] = indexTab[1];
        boolean answer = false;

        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                if((Y[i] != y || X[j] != x) && (Y[i] > -1 && Y[i] < height && X[j] > -1 && X[j] < width)){
                    if(cellTab[Y[i]][X[j]].getGrain().getId() != cellTab[y][x].getGrain().getId()){
                        answer = true;
                    }
                }
            }
        }
        return answer;
    }

    private double countDensity(double time){ //TODO

        return A/B + (1.0 - A / B) * Math.exp(-B * time);
    }

    private int[][] squareCoordinates(int x, int y, int r){
        int X[] = new int[r*2+1];
        int Y[] = new int[r*2+1];

        int a;
        if(bc){
            a = -r;
            for(int i = 0; i < X.length; i++){
                X[i] = (x+a+width)%width;
                a++;
            }

            a = -r;
            for(int i = 0; i < Y.length; i++){
                Y[i] = (y+a+height)%height;
                a++;
            }
        } else { //nie periodyczne
            a = -r;
            for(int i = 0; i < X.length; i++){
                X[i] = (x+a);
                a++;
            }

            a = -r;
            for(int i = 0; i < Y.length; i++){
                Y[i] = (y+a);
                a++;
            }
        }
        int tab[][] = {X, Y};
        return tab;
    }

    private int moorRec(int x, int y){
        int indexTab[][] = squareCoordinates(x,y,1);
        int X[] = indexTab[0];
        int Y[] = indexTab[1];

        Map<Integer,Integer> map = new HashMap<>();
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                if((Y[i] != y || X[j] != x) && (Y[i] > -1 && Y[i] < height && X[j] > -1 && X[j] < width)){
                    if(cellTab[Y[i]][X[j]].isRecrystallized()){
                        Integer mapVal = map.get(cellTab[Y[i]][X[j]].getGrain().getId());
                        if(mapVal != null){
                            mapVal++;
                            map.put(cellTab[Y[i]][X[j]].getGrain().getId(),mapVal);
                        }else{
                            map.put(cellTab[Y[i]][X[j]].getGrain().getId(),1);
                        }
                    }
                }
            }
        }
        int max = 0;
        int maxId = 0;

        for (Map.Entry<Integer, Integer> entry : map.entrySet()){
            if(entry.getValue() > max){
                max = entry.getValue();
                maxId = entry.getKey();
            }
        }
        return maxId;
    }

    public void generateGrains(){
        for(int i = 0; i < height; i ++) {
            for(int j = 0; j < width; j++) {
                addNewGrain(j,i);
            }
        }
        updateView();
    }

    public void initMoneCarlo(){
        int grainsCount = Integer.parseInt(txtStateCount.getText());
        grains.clear();

        for(int i = 0; i < grainsCount; i++){
            if(selectedGrains.get(i+1) == null){
                Grain grain = new Grain(i+1);
                grains.put(i+1,grain);
            }
        }

        for(int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if(cellTab[i][j].getType() == CellType.empty){
                    while (true) {
                        int id = (int)(Math.random() * grainsCount + 1);
                        if(selectedGrains.get(id) == null){
                            cellTab[i][j].setGrain(grains.get(id));
                            break;
                        }
                    }
                }
            }
        }

        grains.putAll(selectedGrains);
        updateView();
    }

    public void MonteCarlo(){
        bc = borderCondition.isSelected();
        iterations = Integer.parseInt(txtIteration.getText());

        //przepisanie
        Cell tmp[][] = new Cell[height][width];
        for(int i = 0; i < height; i ++) {
            for(int j = 0; j < width; j++) {
                tmp[i][j] = cellTab[i][j];
            }
        }

        Thread thread = new Thread( () -> {
                for(int time = 0; time < iterations; time++) {

                    mixTable(tmp);

                    for(int i = 0; i < height; i ++) {
                        for(int j = 0; j < width; j++) {
                            tmp[i][j].setBorder(checkBorder(j,i));
                        }
                    }

                    for(int i = 0; i < height; i ++) {
                        for (int j = 0; j < width; j++) {
                            if(tmp[i][j].isBorder() && tmp[i][j].getType() == CellType.grain){
                                int id = calcualteEnergy(tmp[i][j].getX(),tmp[i][j].getY());
                                if(id != 0){
                                    tmp[i][j].setGrain(grains.get(id));

                                }
                            }
                        }
                    }


                    updateView();
                    try {
                        sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        );
        thread.start();
    }

    private void mixTable(Cell[][] tab){
        for(int a = 0; a < 10; a++){
            for(int i = 0; i < height; i ++) {
                for(int j = 0; j < width; j++) {
                    int x = ((int)(Math.random() * width));
                    int y = ((int)(Math.random() * height));
                    Cell temp = tab[i][j];
                    tab[i][j] = tab[y][x];
                    tab[y][x] = temp;
                }
            }
        }

    }

    private int calcualteEnergy(int x, int y){
        if(cellTab[y][x].getType() == CellType.selected){
            return 0;
        }
        int indexTab[][] = squareCoordinates(x,y,1);
        int X[] = indexTab[0];
        int Y[] = indexTab[1];

        List<Point> cellsList = new ArrayList<>();
        int energy = 0;
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                if((Y[i] != y || X[j] != x) && (Y[i] > -1 && Y[i] < height && X[j] > -1 && X[j] < width)){
                    if(cellTab[Y[i]][X[j]].getGrain().getId() != cellTab[y][x].getGrain().getId() && cellTab[Y[i]][X[j]].getType() != CellType.selected){
                       energy++;
                        Point point = new Point(j, i);
                        cellsList.add(point);
                    }
                }
            }
        }

        if(cellsList.isEmpty()){
            return 0;
        }

        Point randCell = cellsList.get((int)(Math.random() * cellsList.size()));
        int randX = (int)randCell.getX();
        int randY = (int)randCell.getY();

//        int randX = (int)(Math.random() * 2.99);
//        int randY = (int)(Math.random() * 2.99);
        int energy2 = 0;
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                if((Y[i] != y || X[j] != x) && (Y[i] > -1 && Y[i] < height && X[j] > -1 && X[j] < width) && (Y[randY] > -1 && Y[randY] < height && X[randX] > -1 && X[randX] < width)){
                    if(cellTab[Y[i]][X[j]].getGrain().getId() != cellTab[Y[randY]][X[randX]].getGrain().getId()){
                        energy2++;
                    }
                }
            }
        }


        if(energy <= energy2){
            return 0;
        }else{
            if(Y[randY] > -1 && Y[randY] < height && X[randX] > -1 && X[randX] < width){
                return cellTab[Y[randY]][X[randX]].getGrain().getId();
            }else{
                return 0;
            }

        }
    }

    public void exportFile() {
        File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            TImport tImport = new TImport(cellTab, grains, height, width);
            FileExport fileExport = new FileExport(file);
            fileExport.export(tImport);
//            fileExport.exportTxt(cellTab, height, width);
        }

    }

    public void importFile() {
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            FileImport fileImport = new FileImport(file);
            TImport tImport = fileImport.importFile();
            this.createGrid(tImport.getWidth(), tImport.getHeight());
            this.cellTab = tImport.getCells();
            this.height = tImport.getHeight();
            this.width = tImport.getWidth();
            this.grains = tImport.getGrains();
            this.updateView();
        }
    }

    public void addInclusionOnBorder(){
        List<Cell> borderCell = getBorderCell(Integer.parseInt(borderRadius.getText()));
        borderCell.forEach(cell -> cell.setType(CellType.inclusion));

        updateView();
    }

    private List<Cell> getBorderCell(int r) {
        List<Cell> cells = new ArrayList<>();

        for(int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if(checkNeighbors(i, j, r)){
                    cells.add(cellTab[j][i]);
                }
            }
        }
        return cells;
    }

    public void addBorderOnSelectedGrains(){
        int tmp[][] = new int[height][width];


        for(int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if(cellTab[j][i].getType() == CellType.selected && checkNeighbors(i, j, Integer.parseInt(borderRadius.getText()))){
                    tmp[j][i] = 1;
                }
            }
        }

        for(int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if(tmp[j][i] == 1) {
                    cellTab[j][i].setType(CellType.inclusion);
                }
            }
        }
        updateView();
    }

    public void setEnergyToCells() {
        if(energyBorderRadioButton.isSelected()){
            List<Cell> borderCell = getBorderCell(1);

            for(int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    cellTab[j][i].setEnergy(Integer.parseInt(txtEnergyOnBorder.getText()));
                }
            }

            borderCell.forEach(cell -> cell.setEnergy(Integer.parseInt(txtEnergyAnywhere.getText())));
        } else if(energyAllRadioButton.isSelected()){
            for(int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    cellTab[j][i].setEnergy(Integer.parseInt(txtEnergyAnywhere.getText()));
                }
            }
        }
        updateView();
    }

    private void setRecrystalizedGrains(int number) {
        for(int c = 0; c < number; c ++){
            int x = (int)(Math.random() * width);
            int y = (int)(Math.random() * height);


            if(cellTab[y][x].getType() != CellType.recrystallized) {
                addNewGrain(x,y);
                cellTab[y][x].setType(CellType.recrystallized);
                cellTab[y][x].setEnergy(0);
            }

        }
        updateView();
    }

    public void addRecrystalizationGains(int iteration) {
        int count = Integer.parseInt(txtRecrystallizedGrains.getText());

        if((iteration < 1) || radioConstantNucleon.isSelected()){
            setRecrystalizedGrains(count);
        }else if(radioIncreasingNucleon.isSelected()){
            int increasing = Integer.parseInt(txtIncreasingNucleons.getText());
            count = count + increasing * iteration;
            setRecrystalizedGrains(count);
        } else {
            return;
        }
        updateView();
    }

    public void rec() {
        bc = borderCondition.isSelected();
        iterations = Integer.parseInt(txtIteration.getText());

        //przepisanie
        Cell tmp[][] = new Cell[height][width];
        for(int i = 0; i < height; i ++) {
            for(int j = 0; j < width; j++) {
                tmp[i][j] = cellTab[i][j];
            }
        }

        Thread thread = new Thread( () -> {
            for(int time = 0; time < iterations; time++) {

                mixTable(tmp);
                addRecrystalizationGains(time);
                for(int i = 0; i < height; i ++) {
                    for(int j = 0; j < width; j++) {
                        tmp[i][j].setBorder(checkBorder(j,i));
                    }
                }

                for(int i = 0; i < height; i ++) {
                    for (int j = 0; j < width; j++) {
                        if(tmp[i][j].isBorder() && (tmp[i][j].getType() == CellType.grain || tmp[i][j].getType() == CellType.recrystallized || tmp[i][j].getType() == CellType.selected)){
                            int id = calculateEnergyForRecrystallization(tmp[i][j].getX(),tmp[i][j].getY());
                            if(id != 0){
                                tmp[i][j].setGrain(grains.get(id));
                                tmp[i][j].setEnergy(0);
                                tmp[i][j].setType(CellType.recrystallized);

                            }
                        }
                    }
                }


                updateView();
                try {
                    sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        );
        thread.start();
    }

    private int calculateEnergyForRecrystallization(int x, int y){
        if(cellTab[y][x].getType() == CellType.selected){
            return 0;
        }
        int indexTab[][] = squareCoordinates(x,y,1);
        int X[] = indexTab[0];
        int Y[] = indexTab[1];


        List<Point> recrystallizedList = new ArrayList<>();
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                if((Y[i] != y || X[j] != x) && (Y[i] > -1 && Y[i] < height && X[j] > -1 && X[j] < width)){
                    if(cellTab[Y[i]][X[j]].getType() == CellType.recrystallized){
                        Point point = new Point(j, i);
                        recrystallizedList.add(point);
                    }
                }
            }
        }

        if(recrystallizedList.isEmpty()){
            return 0;
        }

        int energy = 0;
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                if((Y[i] != y || X[j] != x) && (Y[i] > -1 && Y[i] < height && X[j] > -1 && X[j] < width)){
                    if(cellTab[Y[i]][X[j]].getGrain().getId() != cellTab[y][x].getGrain().getId()){
                        energy++;
                    }
                }
            }
        }
        energy += cellTab[y][x].getEnergy();

        Point randCell = recrystallizedList.get((int)(Math.random() * recrystallizedList.size()));
        int randX = (int)randCell.getX();
        int randY = (int)randCell.getY();

        int energy2 = 0;
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                if((Y[i] != y || X[j] != x) && (Y[i] > -1 && Y[i] < height && X[j] > -1 && X[j] < width) && (Y[randY] > -1 && Y[randY] < height && X[randX] > -1 && X[randX] < width)){
                    if(cellTab[Y[i]][X[j]].getGrain().getId() != cellTab[Y[randY]][X[randX]].getGrain().getId()){
                        energy2++;
                    }
                }
            }
        }


        if(energy <= energy2){
            return 0;
        }else{
            if(Y[randY] > -1 && Y[randY] < height && X[randX] > -1 && X[randX] < width){
                return cellTab[Y[randY]][X[randX]].getGrain().getId();
            }else{
                return 0;
            }

        }
    }

//    public void importFile() {
//        File file = fileChooser.showOpenDialog(stage);
//        if (file != null) {
//            FileImport fileImport = new FileImport(file);
//            TImport tImport = fileImport.importFromJson();
////            TImport tImport = fileImport.importFile();
//            Cell[][] cells = tImport.getCells();
//            this.createGrid(tImport.getWidth(), tImport.getHeight());
//            this.cellTab = tImport.getCells();
//            this.height = tImport.getHeight();
//            this.width = tImport.getWidth();
//            this.grains = tImport.getGrains();
//            this.updateView();
//        }
//    }


}
