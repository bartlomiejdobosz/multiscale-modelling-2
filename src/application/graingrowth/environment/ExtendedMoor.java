package application.graingrowth.environment;

import java.util.Map;

public class ExtendedMoor extends Environment {
    @Override
    public int count(int x, int y, int r) {

        return getId(x,y);
    }

    @Override
    public Map<Integer, Integer> count(int x, int y) {

        return null;
    }

    private int getId(int x, int y) {
        Environment env = new Moor();
        env.init(width, height, bc, cellTab);


        Map<Integer, Integer> moor = env.count(x, y);
        int result = checkCount(moor, 5);
        if(result > 0) return result;

        env = new VonNeuman();
        env.init(width, height, bc, cellTab);
        Map<Integer, Integer> count = env.count(x, y);
        result = checkCount(count, 3);
        if(result > 0) return result;

        env = new MoorCorners();
        env.init(width, height, bc, cellTab);
        count = env.count(x, y);
        result = checkCount(count, 3);
        if(result > 0)return result;

        int maxId = maxId(moor);
        double d = Math.random();
        if (d < moorCornersPercent) {
            return maxId;
        }

        return 0;
    }

    private int checkCount(Map<Integer, Integer> map, int number) {
        for (Map.Entry<Integer, Integer> entry : map.entrySet()){
            if(entry.getValue() >= number){
                return entry.getKey();
            }
        }
        return 0;
    }
}
