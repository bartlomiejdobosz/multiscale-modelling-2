package application.graingrowth.environment;

import application.graingrowth.utils.Cell;
import application.graingrowth.utils.CellType;

import java.util.Map;

public abstract class Environment {
    int width;
    int height;
    boolean bc;
    double moorCornersPercent;
    Cell cellTab[][];

    public void init(int width, int height, boolean bc, Cell cellTab[][]){
        this.width = width;
        this.height = height;
        this.bc = bc;
        this.cellTab = cellTab;
    }

    public abstract int count(int x, int y, int r);

    public abstract Map<Integer, Integer> count(int x, int y);

    public int maxId(Map<Integer,Integer> map){
        int max = 0;
        int maxId = 0;

        for (Map.Entry<Integer, Integer> entry : map.entrySet()){
            if(entry.getValue() > max){
                max = entry.getValue();
                maxId = entry.getKey();
            }
        }
        return maxId;
    }

    public int[][] squareCoordinates(int x, int y, int r){
        int X[] = new int[r*2+1];
        int Y[] = new int[r*2+1];

        int a;
        if(bc){
            a = -r;
            for(int i = 0; i < X.length; i++){
                X[i] = (x+a+width)%width;
                a++;
            }

            a = -r;
            for(int i = 0; i < Y.length; i++){
                Y[i] = (y+a+height)%height;
                a++;
            }
        } else { //nie periodyczne
            a = -r;
            for(int i = 0; i < X.length; i++){
                X[i] = (x+a);
                a++;
            }

            a = -r;
            for(int i = 0; i < Y.length; i++){
                Y[i] = (y+a);
                a++;
            }
        }
        int tab[][] = {X, Y};
        return tab;
    }

    protected void addToMap(Map<Integer,Integer> map, int x, int y){
        if(cellTab[y][x].getType() == CellType.grain || cellTab[y][x].getType() == CellType.selected){
            Integer mapVal = map.get(cellTab[y][x].getGrain().getId());
            if(mapVal != null){
                mapVal++;
                map.put(cellTab[y][x].getGrain().getId(),mapVal);
            }else{
                map.put(cellTab[y][x].getGrain().getId(),1);
            }
        }
    }

    public double getMoorCornersPercent() {
        return moorCornersPercent;
    }

    public void setMoorCornersPercent(double moorCornersPercent) {
        this.moorCornersPercent = moorCornersPercent;
    }
}
