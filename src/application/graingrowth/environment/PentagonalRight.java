package application.graingrowth.environment;


import java.util.HashMap;
import java.util.Map;

public class PentagonalRight extends Environment{
    @Override
    public int count(int x, int y, int r) {
        int indexTab[][] = squareCoordinates(x,y,1);
        int X[] = indexTab[0];
        int Y[] = indexTab[1];

        Map<Integer,Integer> map = new HashMap<>();
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                if((Y[i] != y || X[j] != x) && (j != 2) && (Y[i] > -1 && Y[i] < height && X[j] > -1 && X[j] < width)){
                    addToMap(map,X[j],Y[i]);
                }
            }
        }
        return maxId(map);
    }

    @Override
    public Map<Integer, Integer> count(int x, int y) {
        return null;
    }
}
