package application.graingrowth.environment;

import java.util.HashMap;
import java.util.Map;

public class RandomAutomats extends Environment {
    @Override
    public int count(int x, int y, int r) {
        int indexTab[][] = squareCoordinates(x,y,r);
        int X[] = indexTab[0];
        int Y[] = indexTab[1];

        Map<Integer,Integer> map = new HashMap<>();
        for(int i = 0; i < Y.length; i++){
            for(int j = Math.abs(r - i); j < X.length - Math.abs(r - i); j++){
                if((Y[i] > -1) && (Y[i] < height) && (X[j] > -1) && (X[j] < width)){
                    if(cellTab[Y[i]][X[j]].getGrain() != null){
                        addToMap(map,X[j],Y[i]);
                    }
                }
            }
        }
        return maxId(map);
    }

    @Override
    public Map<Integer, Integer> count(int x, int y) {
        return null;
    }
}
